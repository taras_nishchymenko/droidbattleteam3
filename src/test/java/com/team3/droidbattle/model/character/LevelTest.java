package com.team3.droidbattle.model.character;

import static org.junit.Assert.*;

import org.junit.Test;

public class LevelTest {

  @Test
  public void nextLevelTest() {
    Level level = new Level();
    assertEquals(1, level.getLevel());
    assertEquals(100, level.getToNextLevel());
    level.increaseXp(32);
    assertEquals(32, level.getXp());
    assertEquals(1, level.increaseXp(68));
    assertEquals(0, level.getXp());
    assertEquals(200, level.getToNextLevel());
    level.increaseXp(220);
    assertEquals(20, level.getXp());
  }
}