package com.team3.droidbattle.model.character.main;

import com.team3.droidbattle.model.character.Attack;
import com.team3.droidbattle.model.item.weapon.Blaster;
import com.team3.droidbattle.model.item.weapon.Stick;
import com.team3.droidbattle.model.item.weapon.Sword;
import org.junit.Test;

public class MainCharacterTest {

  @Test
  public void mainCharacterTest() {
    Mage mage = new Mage("name");
    mage.increaseXp(1000);

    mage.put(new Sword(5));
    mage.put(new Stick(5));

    Warrior warrior = new Warrior("name");
    warrior.increaseXp(1000);

    warrior.put(new Blaster(5));
    warrior.put(new Sword(5));

    Shooter shooter = new Shooter("name");
    shooter.increaseXp(1000);

    shooter.put(new Stick(5));
    shooter.put(new Blaster(5));
  }

  private void showStats(FullStats stats) {
    System.out.println(stats);
  }

  @Test
  public void levelsStatsTest() {
    Mage mage = new Mage("mage");
    showStats(mage.getStats());
    mage.increaseXp(100);
    showStats(mage.getStats());
    mage.increaseXp(200);
    showStats(mage.getStats());
    mage.increaseXp(400);
    showStats(mage.getStats());
    mage.increaseXp(800);
    showStats(mage.getStats());
  }

  @Test
  public void critTest() {
    Shooter shooter = new Shooter("name");
    shooter.increaseXp(1000000000);
    shooter.put(new Blaster(17));

    int shooterCrit = 0;
    int damage = 0;
    int iterations = 10_000;
    for (int i = 0; i < iterations; i++) {
      Attack result = shooter.attack();
      damage += result.getAttackPower();
      if (result.isCrit()) {
        shooterCrit++;
      }
    }
    System.out.println("shooter crits count: " + shooterCrit);
    System.out.println("awg dmg: " + damage/iterations);

    Mage mage = new Mage("name");
    mage.increaseXp(1000000000);
    mage.put(new Stick(17));

    int mageCrit = 0;
    damage = 0;
    for (int i = 0; i < iterations; i++) {
      Attack result = mage.attack();
      damage += result.getAttackPower();
      if (result.isCrit()) {
        mageCrit++;
      }
    }
    System.out.println("shooter crits count: " + mageCrit);
    System.out.println("awg dmg: " + damage/iterations);
  }
}