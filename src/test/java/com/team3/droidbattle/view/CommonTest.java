package com.team3.droidbattle.view;

import com.team3.droidbattle.model.Chest;
import com.team3.droidbattle.model.Drawable;
import com.team3.droidbattle.model.FreeSpace;
import com.team3.droidbattle.model.Tree;
import com.team3.droidbattle.model.character.enemy.DuckDroid;
import com.team3.droidbattle.model.character.enemy.Droid;
import com.team3.droidbattle.model.character.enemy.Humanoid;
import com.team3.droidbattle.model.character.main.Mage;
import com.team3.droidbattle.model.character.main.Shooter;
import com.team3.droidbattle.model.character.main.Warrior;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

public class CommonTest {

  @Test
  public void commonTest() {

    List<Drawable> drawables = new ArrayList<>();

    DuckDroid duckDroid = new DuckDroid(1);
    Droid droid = new Droid(1);
    Humanoid humanoid = new Humanoid(1);
    Mage mage = new Mage("mage");
    Shooter shooter = new Shooter("shooter");
    Warrior warrior = new Warrior("warrior");
    Chest chest = new Chest(10);
    FreeSpace freeSpace = new FreeSpace();
    Tree tree = new Tree();

    drawables.add(duckDroid);
    drawables.add(droid);
    drawables.add(humanoid);
    drawables.add(mage);
    drawables.add(shooter);
    drawables.add(warrior);
//    drawables.add(chest);
    drawables.add(freeSpace);
    drawables.add(tree);

    for (Drawable drawable : drawables) {
      for (String lines : drawable.getDrawInstance().getLines()) {
        System.out.println(lines);
      }
    }
  }

}
