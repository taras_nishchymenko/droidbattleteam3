package com.team3.droidbattle.view;

import com.team3.droidbattle.model.FreeSpace;
import com.team3.droidbattle.model.character.enemy.Droid;
import com.team3.droidbattle.model.character.enemy.DuckDroid;
import com.team3.droidbattle.model.character.enemy.Humanoid;
import com.team3.droidbattle.model.character.main.Mage;
import com.team3.droidbattle.model.character.main.Shooter;
import com.team3.droidbattle.model.character.main.Warrior;
import java.util.Arrays;
import org.junit.Test;

public class NodePanelTest {

  @Test
  public void nodePanelViewTest() {
    NodePanel panel = new NodePanel();
    Node node1 = new Node(Arrays.asList(new FreeSpace()));
    Node node2 = new Node(Arrays.asList(new FreeSpace(),new Mage("Mage")));
    Node node3 = new Node(Arrays.asList(new FreeSpace(),new Warrior("Warrior")));
    Node node4 = new Node(Arrays.asList(new FreeSpace(),new Shooter("Shooter")));
    Node node5 = new Node(Arrays.asList(new FreeSpace(),new Droid(1)));
    Node node6 = new Node(Arrays.asList(new FreeSpace(),new DuckDroid(1)));
    Node node7 = new Node(Arrays.asList(new FreeSpace(),new Humanoid(1)));
    panel.setNodes(Arrays.asList(node1, node2, node3, node4, node5, node6, node7));
    for (String line : panel.getNodesView()) {
      System.out.println(line);
    }

  }

}