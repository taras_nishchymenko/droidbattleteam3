package com.team3.droidbattle.view;

import static org.junit.Assert.*;

import com.team3.droidbattle.model.FreeSpace;
import com.team3.droidbattle.model.Tree;
import com.team3.droidbattle.model.character.main.Mage;
import java.util.Arrays;
import org.junit.Test;

public class NodeTest {

  @Test
  public void nodeTest() {
    Node node = new Node(Arrays.asList(new FreeSpace(), new Tree(), new Mage("mage")));
    for (String line : node.getNodeView()) {
      System.out.println(line);
    }
  }

}