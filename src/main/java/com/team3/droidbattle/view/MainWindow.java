package com.team3.droidbattle.view;

import com.team3.droidbattle.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class MainWindow {

  private NodePanel nodePanel = new NodePanel();
  private static final int WIDTH = 233;

  public void setDrawableNodes(List<Node> nodes) {
    nodePanel.setNodes(nodes);
  }

  public List<String> mainWindow() {
    List<String> result = new ArrayList<>();

    String line = StringUtil.rightPadTillSize("*", WIDTH, '*');
    result.add(line);

    String current;
    for (String string : nodePanel.getNodesView()) {
      current = "*" + string;
      current = StringUtil.rightPadTillSize(current, WIDTH - 1, ' ');
      current += "*";
      result.add(current);
    }
    result.add(line);

    return result;
  }

}
