package com.team3.droidbattle.view;

import java.util.ArrayList;
import java.util.List;

public class NodePanel {

  private List<Node> nodes;

  public List<String> getNodesView() {
    List<String> result = new ArrayList<>();

    for (int i = 0; i < nodes.get(0).getNodeView().size(); i++) {
      StringBuilder line = new StringBuilder();
      for (Node node : nodes) {
        line.append(node.getNodeView().get(i));
      }
      result.add(new String(line));
    }

    return result;
  }

  public List<Node> getNodes() {
    return nodes;
  }

  public void setNodes(List<Node> nodes) {
    this.nodes = nodes;
  }
}
