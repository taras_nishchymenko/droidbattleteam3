package com.team3.droidbattle.view;

import com.team3.droidbattle.model.Drawable;
import com.team3.droidbattle.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class Node {

  private List<Drawable> drawableList;

  public Node(List<Drawable> drawableList) {
    this.drawableList = drawableList;
  }

  public List<String> getNodeView() {
    List<String> lines = drawableList.get(0).getDrawInstance().getLines();

    for (int i = 1; i < drawableList.size(); i++) {
      lines = drawAbove(lines, drawableList.get(i));
    }

    return lines;
  }

  private List<String> drawAbove(List<String> lines, Drawable drawable) {

    List<String> above = drawable.getDrawInstance().getLines();
    List<String> result = new ArrayList<>();

    String newLine;

    for (int i = 0; i < lines.size() && i < above.size(); i++) {
      char[] line = lines.get(i).toCharArray();
      char[] lineAbove = above.get(i).toCharArray();
      for (int j = 0; j < line.length - 1; j++) {
        if (lineAbove[j] != ' ') {
          line[j] = lineAbove[j];
        }
      }
      newLine = new String(line);
      result.add(newLine);
    }

    if (above.size() < lines.size()) {
      result.addAll(above.size(), lines.subList(above.size(), lines.size()-1));
    }

    return result;
  }

  public List<Drawable> getDrawableList() {
    return drawableList;
  }

  public void setDrawableList(List<Drawable> drawableList) {
    this.drawableList = drawableList;
  }
}
