package com.team3.droidbattle.view;

import static com.team3.droidbattle.model.character.main.CharacterType.MAGE;
import static com.team3.droidbattle.model.character.main.CharacterType.SHOOTER;
import static com.team3.droidbattle.model.character.main.CharacterType.WARRIOR;

import com.team3.droidbattle.model.character.main.CharacterType;
import java.util.Scanner;

public class WelcomePanel {

  private String frame = "+--------------------------------+";
  private String nameOfCharacter = "";
  private CharacterType type;


  public WelcomePanel() {

    showWelcome(nameOfCharacter);
    System.out.println("Enter your nick name for chraracter, please: ");
    nameOfCharacter = scanNameOfCharacter();
    showWelcome(nameOfCharacter);
    System.out.println("Enter type of character, please:");
    showDescription();
    type = getTypeOfCharacter();

  }

  public String getNameOfCharacter() {
    return nameOfCharacter;
  }

  public CharacterType getCharacterType() {
    return type;
  }

  public int scanTypeofCharacter() {
    int n = 0;
    Scanner sc = new Scanner(System.in);

    try {
      n = sc.nextInt();
      if (n <= 0 || n > 3) {
        System.out.println("Wrong character!");
        scanTypeofCharacter();
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }

    return n;
  }

  public String scanNameOfCharacter() {

    String name = "";
    Scanner sc = new Scanner(System.in);

    try {
      if (sc.hasNext()) {
        name = sc.nextLine();
      } else {
        System.out.println("Empty name!");
        scanNameOfCharacter();
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
    return name;
  }

  public CharacterType getTypeOfCharacter() {
    int type = scanTypeofCharacter();
    switch (type) {
      case 1:
        return MAGE;
      case 2:
        return SHOOTER;
      case 3:
        return WARRIOR;
      default:
        return MAGE;
    }
  }

  public void showWelcome(String nameOfCharacter) {
    String droid = "DROID BATTLE";

    if (nameOfCharacter == "") {
      System.out.println("                           " + frame);
      System.out.print("                           |");
      for (int i = 32; i > 0; i--) {
        System.out.print(" ");
      }
      System.out.println("|");
      System.out.print("                           |          ");
      System.out.print(droid);
      System.out.println("          |");
      System.out.print("                           |");
      for (int i = 32; i > 0; i--) {
        System.out.print(" ");
      }
      System.out.println("|");
      System.out.println("                           " + frame);
    } else {
      System.out.println("                           " + frame);
      System.out.print("                           |");
      for (int i = 32; i > 0; i--) {
        System.out.print(" ");
      }
      System.out.println("|");
      System.out.print("                           |       ");
      System.out.print("WELCOME, ");
      System.out.print(nameOfCharacter + "!");
      for (int j = 34 - (nameOfCharacter.length() + 19); j > 0; j--) {
        System.out.print(" ");
      }
      System.out.println("|");
      System.out.print("                           |");
      for (int i = 32; i > 0; i--) {
        System.out.print(" ");
      }
      System.out.println("|");
      System.out.println("                           " + frame);

    }


  }

  public void showDescription() {

    System.out.println("+-----------------------------------------------+");
    System.out.println("|  1 - Magician                                 |");
    System.out.println("|   Wizards are considered to be spellcasters   |\n"
        + "|   who wield power-ful spells, but are often   |\n"
        + "|   physically weak as a trade-off              |");
    System.out.println("|                                               |");
    System.out.println("|  2 - Shoter                                   |");
    System.out.println("|   The arrows are able to kill the enemy       |\n"
        + "|   droid at a distance                         |");
    System.out.println("|                                               |");
    System.out.println("|  3 - Warrior                                  |");
    System.out.println("|   Warrior specializing in combat or warfare,  |\n"
        + "|   and definitely well possesses a sword       |");
    System.out.println("+-----------------------------------------------+");
  }

}
