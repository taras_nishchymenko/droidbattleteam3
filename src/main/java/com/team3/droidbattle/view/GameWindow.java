package com.team3.droidbattle.view;

import java.util.Collections;
import java.util.List;

public class GameWindow {

  private Window movesWindow = new Window(101, 15);
  private Window characterStatsWindow = new Window(66, 15);
  private Window itemStatsWindow = new Window(66, 15);
  private Window messageWindow = new Window(233, 4);
  private MainWindow mainWindow = new MainWindow();

  public String gameWindow() {
    StringBuilder builder = new StringBuilder();
    for (String string : mainWindow.mainWindow()) {
      builder.append(string);
      builder.append('\n');
    }
    for (int i = 0; i < movesWindow.getWindow().size(); i++) {
      builder.append(movesWindow.getWindow().get(i));
      builder.append(characterStatsWindow.getWindow().get(i));
      builder.append(itemStatsWindow.getWindow().get(i));
      builder.append('\n');
    }
    for (String string : messageWindow.getWindow()) {
      builder.append(string);
      builder.append('\n');
    }
    return new String(builder);
  }

  public void setMoves(List<String> moves) {
    movesWindow.setStrings(moves);
  }

  public void setCharacterStats(List<String> stats) {
    characterStatsWindow.setStrings(stats);
  }

  public void setEnemyStats(List<String> stats) {
    itemStatsWindow.setStrings(stats);
  }

  public void setItemStats(List<String> stats) {
    itemStatsWindow.setStrings(stats);
  }

  public void setNodes(List<Node> nodes) {
    mainWindow.setDrawableNodes(nodes);
  }

  public void setMessage(String message) {
    messageWindow.setStrings(Collections.singletonList(message));
  }
}
