package com.team3.droidbattle.view;

import com.team3.droidbattle.util.StringUtil;
import java.util.ArrayList;
import java.util.List;


public class Window {

  private List<String> strings ;
  private int width;
  private int height;


  public Window(int width, int height) {
    this.width = width;
    this.height = height;
  }


  public void setStrings(List<String> strings) {
    this.strings = strings;

  }

  public List<String> getWindow() {

    List<String> result = new ArrayList<>();

    String line = StringUtil.rightPadTillSize("*", width, '*');
    result.add(line);

    String current;
    for (String string : strings) {
      current = "*" + string;
      current = StringUtil.rightPadTillSize(current, width - 1, ' ');
      current += "*";
      result.add(current);
    }

    while (result.size() < height - 1) {
      current = "*";
      current = StringUtil.rightPadTillSize(current, width - 1, ' ');
      current += "*";
      result.add(current);
    }

    result.add(line);

    return result;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }
}
