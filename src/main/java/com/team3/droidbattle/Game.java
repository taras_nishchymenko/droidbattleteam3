package com.team3.droidbattle;


import com.team3.droidbattle.controller.GameContext;

public class Game {

  public static void main(String[] args) {
    GameContext gameContext = new GameContext();
    gameContext.start();
  }
}
