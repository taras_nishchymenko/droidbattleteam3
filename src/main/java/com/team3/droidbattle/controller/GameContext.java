package com.team3.droidbattle.controller;

import com.team3.droidbattle.model.Chest;
import com.team3.droidbattle.model.Drawable;
import com.team3.droidbattle.model.FreeSpace;
import com.team3.droidbattle.model.character.Attack;
import com.team3.droidbattle.model.character.AttackResult;
import com.team3.droidbattle.model.character.enemy.Droid;
import com.team3.droidbattle.model.character.enemy.DuckDroid;
import com.team3.droidbattle.model.character.enemy.Enemy;
import com.team3.droidbattle.model.character.enemy.Humanoid;
import com.team3.droidbattle.model.character.main.FullStats;
import com.team3.droidbattle.model.character.main.MainCharacter;
import com.team3.droidbattle.model.item.Heal;
import com.team3.droidbattle.model.item.Item;
import com.team3.droidbattle.model.item.armor.ArmorItem;
import com.team3.droidbattle.model.item.armor.ArmorItemStats;
import com.team3.droidbattle.model.item.weapon.WeaponItem;
import com.team3.droidbattle.model.item.weapon.WeaponItemStats;
import com.team3.droidbattle.util.Random;
import com.team3.droidbattle.view.GameWindow;
import com.team3.droidbattle.view.Node;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class GameContext {

  private MainCharacter mainCharacter;
  private Chest chest;
  private List<Node> nodes;
  private GameWindow gameWindow;
  private boolean enemyInGame;
  private boolean lootInGame;
  private Enemy enemy;
  private static final List<String> NOTHING_TO_SHOW = Arrays.asList("Nothing to show");
  private static final List<String> MOVE = Arrays.asList(" 1 - Move", " q - Exit");
  private static final List<String> PICK_ITEM = Arrays.asList(" 1 - Move", " 3 - Pick", " 4 - Take a heal", " q - Exit");
  private static final List<String> ATTACK = Arrays.asList(" 1 - Move", " 2 - Attack", " q - Exit");

  public void start() {
    mainCharacter = new UserInitializer().initialize();
    gameWindow = new GameWindow();
    gameWindow.setCharacterStats(getCharacterStats());
    gameWindow.setMessage("Game started!");
    chest = new Chest(1);
    gameWindow.setItemStats(getItemStats());
    gameWindow.setMoves(MOVE);
    firstNodes();
    gameWindow.setNodes(nodes);
    enemyInGame = false;
    lootInGame = true;

    showGameWindow();

    startGameProcess();
  }

  private List<String> getItemStats() {
    Item item = chest.getLoot();
    Heal heal = chest.getHeal();

    List<String> result = new ArrayList<>();

    if (item instanceof ArmorItem) {
      ArmorItemStats stats = ((ArmorItem) item).getItemStats();
      result.addAll(Arrays.asList(
          item.getType().toString() + " level: " + stats.getLevel(),
          "Power: " + stats.getPower(),
          "HP: " + stats.getHp(),
          "Agility: " + stats.getAgility(),
          "Brain: " + stats.getBrain()
      ));
    } else if (item instanceof WeaponItem) {
      WeaponItemStats stats = ((WeaponItem) item).getItemStats();
      result.addAll(Arrays.asList(
          item.getType().toString() + " level: " + stats.getLevel(),
          "Damage: " + stats.getMinDamage() + "/" + stats.getMaxDamage(),
          "Crit chance: " + stats.getCritDamageChance() + "%",
          "Crit damage: " + stats.getCritDamage(),
          "Distance: " + stats.getDistance(),
          "Type: " + ((WeaponItem) item).getAttackType()
      ));
    }
    if (heal != null) {
      result.add("Heal " + heal.getHpPercent() + "%");
    }
    return result.size() > 0 ? result : NOTHING_TO_SHOW;
  }

  private void startGameProcess() {
    Scanner scanner = new Scanner(System.in);
    while (true) {
      String input = scanner.nextLine();
      if (input.equals("q")) {
        endGame();
      }
      if (input.equals("1")) {
        moveCharacter();
      }
      if (input.equals("2")) {
        attackEnemy();
      }
      if (input.equals("3")) {
        pickItem();
      }
      if (input.equals("4")) {
        takeHeal();
      }
      if (enemyInGame) {
        enemyTurn();
        gameWindow.setCharacterStats(getCharacterStats());
      }
    }
  }

  private void attackEnemy() {
    Attack attack = mainCharacter.attack();
    AttackResult result = enemy.getAttack(attack);
    String message = "";
    if (result.isMiss()) {
      message = "Miss!";
    } else if (result.getXp() > 0) {
      message = "Enemy is dead! Get xp = " + result.getXp();
      mainCharacter.increaseXp(result.getXp());
      gameWindow.setMoves(MOVE);
      enemyInGame = false;
      enemy = null;
      replaceEnemy(new Chest(mainCharacter.getLevel()), message);
      return;
    } else {
      message = "Effectivity: " + result.getEffectivity() + " Damage caused = " + result
          .getDamageCaused();
    }
    gameWindow.setMessage(message);
    gameWindow.setEnemyStats(getEnemyStats());
    showGameWindow();
  }

  private void replaceEnemy(Chest chest, String mesaage) {
    for (int i = 2; i < 7; i++) {
      if (nodes.get(i).getDrawableList().size() > 1 && nodes.get(i).getDrawableList()
          .get(1) instanceof Enemy) {
        nodes.get(i).getDrawableList().set(1, chest);
        this.chest = chest;
        lootInGame = true;
        gameWindow.setMessage(mesaage +" Pick your reward!");
        gameWindow.setItemStats(getItemStats());
        showGameWindow();
      }
    }
  }

  private void takeHeal() {
    mainCharacter.heal(chest.getHeal());
    gameWindow.setCharacterStats(getCharacterStats());
    chest.setHeal(null);
    gameWindow.setItemStats(getItemStats());
    if (chest.getLoot() != null) {
      gameWindow.setMessage("Take a loot!");
    } else {
      gameWindow.setMessage("Move forward");
      lootInGame = false;
    }
    showGameWindow();
  }

  private void pickItem() {
    mainCharacter.put(chest.getLoot());
    gameWindow.setCharacterStats(getCharacterStats());
    chest.setLoot(null);
    gameWindow.setItemStats(getItemStats());
    if (chest.getHeal() != null) {
      gameWindow.setMessage("Take a heal!");
    } else {
      gameWindow.setMessage("Move forward");
      lootInGame = false;
    }
    showGameWindow();
  }

  private void enemyTurn() {

  }

  private void moveCharacter() {
    int size = nodes.get(2).getDrawableList().size();

    if (size > 1 && nodes.get(2).getDrawableList().get(1) instanceof Enemy) {
      gameWindow.setMessage("Enemy is HERE!");
      showGameWindow();
    } else {
      size = nodes.get(3).getDrawableList().size();

      if (size > 1 && nodes.get(3).getDrawableList().get(1) instanceof Chest) {
        gameWindow.setMessage("Pick item or take a heal");
        gameWindow.setMoves(PICK_ITEM);
      }
      size = nodes.get(2).getDrawableList().size();
      if (size > 1 && nodes.get(2).getDrawableList().get(1) instanceof Chest) {
        gameWindow.setMessage("Move forward");
        gameWindow.setMoves(MOVE);
        gameWindow.setItemStats(NOTHING_TO_SHOW);
        chest = null;
        lootInGame = false;
      }
      if (enemyInAttackRange()) {
        gameWindow.setMoves(ATTACK);
        gameWindow.setMessage("Attack the enemy");
      }

      nodes.set(2, nodes.get(3));
      nodes.set(3, nodes.get(4));
      nodes.set(4, nodes.get(5));
      nodes.set(5, nodes.get(6));
      nodes.set(6, nextNode());

      showGameWindow();
    }
  }

  private boolean enemyInAttackRange() {
    int characterRange = mainCharacter.getRange();
    for (int i = 2; i < characterRange + 2 && i < 7; i++) {
      if (nodes.get(i).getDrawableList().size() > 1 && nodes.get(i).getDrawableList()
          .get(1) instanceof Enemy) {
        return true;
      }
    }
    return false;
  }

  private Node nextNode() {
    if (lootInGame || enemyInGame) {
      return new Node(Arrays.asList(new FreeSpace()));
    } else {
      int random = Random.inRange(1, 4);
      switch (random) {
        case 1:
          lootInGame = true;
          chest = new Chest(mainCharacter.getLevel());
          gameWindow.setItemStats(getItemStats());
          return new Node(Arrays.asList(new FreeSpace(), chest));
        case 2:
          enemyInGame = true;
          enemy = new Droid(mainCharacter.getLevel());
          gameWindow.setEnemyStats(getEnemyStats());
          return new Node(Arrays.asList(new FreeSpace(), enemy));
        case 3:
          enemyInGame = true;
          enemy = new DuckDroid(mainCharacter.getLevel());
          gameWindow.setEnemyStats(getEnemyStats());
          return new Node(Arrays.asList(new FreeSpace(), enemy));
        case 4:
          enemyInGame = true;
          enemy = new Humanoid(mainCharacter.getLevel());
          gameWindow.setEnemyStats(getEnemyStats());
          return new Node(Arrays.asList(new FreeSpace(), enemy));
      }
    }
    return new Node(Arrays.asList(new FreeSpace()));
  }

  private List<String> getEnemyStats() {
    FullStats stats = enemy.getFullStats();
    return Arrays.asList(
        enemy.getName() + " level: " + enemy.getLevel(),
        "HP: " + stats.getHp() + "/" + stats.getMaxHp(),
        "Damage: " + stats.getMinDamage() + "/" + stats.getMaxDamage(),
        "Crit chance: " + stats.getCritDamageChance() + "%",
        "Crit damage: " + stats.getCritDamage(),
        "Attack distance: " + stats.getDistance(),
        "Agility: " + stats.getAgility(),
        "Brain: " + stats.getBrain(),
        "Power: " + stats.getPower()
    );
  }

  private void endGame() {
    gameWindow.setMessage("Bye!");
    System.exit(0);
  }

  private void showGameWindow() {
    System.out.print("\033[H\033[2J");
    System.out.flush();
    System.out.println(gameWindow.gameWindow());
  }

  private void firstNodes() {
    Node node1 = new Node(Arrays.asList(new FreeSpace()));
    Node node2 = new Node(Arrays.asList(new FreeSpace(), mainCharacter));
    Node node3 = new Node(Arrays.asList(new FreeSpace()));
    Node node4 = new Node(Arrays.asList(new FreeSpace()));
    Node node5 = new Node(Arrays.asList(new FreeSpace()));
    Node node6 = new Node(Arrays.asList(new FreeSpace(), chest));
    Node node7 = new Node(Arrays.asList(new FreeSpace()));

    nodes = Arrays.asList(node1, node2, node3, node4, node5, node6, node7);
  }

  private List<String> getCharacterStats() {
    FullStats stats = mainCharacter.getStats();
    return Arrays.asList(
        mainCharacter.getName() + " level: " + mainCharacter.getLevel(),
        "HP: " + stats.getHp() + "/" + stats.getMaxHp(),
        "Damage: " + stats.getMinDamage() + "/" + stats.getMaxDamage(),
        "Crit chance: " + stats.getCritDamageChance() + "%",
        "Crit damage: " + stats.getCritDamage(),
        "Attack distance: " + stats.getDistance(),
        "Agility: " + stats.getAgility(),
        "Brain: " + stats.getBrain(),
        "Power: " + stats.getPower()
    );
  }
}
