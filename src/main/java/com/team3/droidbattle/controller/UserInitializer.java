package com.team3.droidbattle.controller;

import com.team3.droidbattle.model.character.main.CharacterType;
import com.team3.droidbattle.model.character.main.Mage;
import com.team3.droidbattle.model.character.main.MainCharacter;
import com.team3.droidbattle.model.character.main.Shooter;
import com.team3.droidbattle.model.character.main.Warrior;
import com.team3.droidbattle.view.WelcomePanel;

public class UserInitializer {

  public MainCharacter initialize() {
    WelcomePanel panel = new WelcomePanel();
    String name = panel.getNameOfCharacter();
    CharacterType type = panel.getCharacterType();
    panel.showWelcome(name);
    switch (type) {
      case MAGE:
        return new Mage(name);
      case SHOOTER:
        return new Shooter(name);
      case WARRIOR:
        return new Warrior(name);

        default:
          return null;
    }
  }
}
