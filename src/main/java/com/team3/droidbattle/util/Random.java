package com.team3.droidbattle.util;

import static java.util.concurrent.ThreadLocalRandom.current;

public class Random {

  public static int inRange(int min, int max) {
    return current().nextInt(min, max + 1);
  }

  public static int inRange(double min, double max) {
    return current().nextInt((int)min, (int) (max + 1));
  }
}
