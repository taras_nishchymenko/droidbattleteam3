package com.team3.droidbattle.util;

public class StringUtil {

  public static String rightPadTillSize(String line, int size, char character) {
    StringBuilder lineBuilder = new StringBuilder(line);
    while (lineBuilder.length() < size) {
      lineBuilder.append(character);
    }
    return lineBuilder.toString();
  }

}
