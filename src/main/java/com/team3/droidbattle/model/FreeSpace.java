package com.team3.droidbattle.model;

public class FreeSpace implements Drawable {

  private static final String file = "/src/main/resources/view/freespace.txt";

  @Override
  public DrawInstance getDrawInstance() {
    return DrawInstanceFactory.get(file);
  }
}
