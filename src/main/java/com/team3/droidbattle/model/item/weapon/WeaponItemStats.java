package com.team3.droidbattle.model.item.weapon;

public class WeaponItemStats {

  private int level;
  private int maxDamage;
  private int minDamage;
  private int distance;
  private int critDamage;
  private int critDamageChance;

  public WeaponItemStats(int level, int maxDamage, int minDamage, int distance, int critDamage,
      int critDamageChance) {
    this.level = level;
    this.maxDamage = maxDamage;
    this.minDamage = minDamage;
    this.distance = distance;
    this.critDamage = critDamage;
    this.critDamageChance = critDamageChance;
  }

  public int getMaxDamage() {
    return maxDamage;
  }

  public int getMinDamage() {
    return minDamage;
  }

  public int getDistance() {
    return distance;
  }

  public int getCritDamage() {
    return critDamage;
  }

  public int getCritDamageChance() {
    return critDamageChance;
  }

  public int getLevel() {
    return level;
  }


  public static final class WeaponItemStatsBuilder {

    private int level;
    private int maxDamage;
    private int minDamage;
    private int distance;
    private int critDamage;
    private int critDamageChance;

    private WeaponItemStatsBuilder() {
    }

    public static WeaponItemStatsBuilder aWeaponItemStats() {
      return new WeaponItemStatsBuilder();
    }

    public WeaponItemStatsBuilder withLevel(int level) {
      this.level = level;
      return this;
    }

    public WeaponItemStatsBuilder withMaxDamage(int maxDamage) {
      this.maxDamage = maxDamage;
      return this;
    }

    public WeaponItemStatsBuilder withMinDamage(int minDamage) {
      this.minDamage = minDamage;
      return this;
    }

    public WeaponItemStatsBuilder withDistance(int distance) {
      this.distance = distance;
      return this;
    }

    public WeaponItemStatsBuilder withCritDamage(int critDamage) {
      this.critDamage = critDamage;
      return this;
    }

    public WeaponItemStatsBuilder withCritDamageChance(int critDamageChance) {
      this.critDamageChance = critDamageChance;
      return this;
    }

    public WeaponItemStats build() {
      return new WeaponItemStats(level, maxDamage, minDamage, distance, critDamage,
          critDamageChance);
    }
  }
}
