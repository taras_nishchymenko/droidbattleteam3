package com.team3.droidbattle.model.item.weapon;

import com.team3.droidbattle.model.DrawInstance;
import com.team3.droidbattle.model.DrawInstanceFactory;
import com.team3.droidbattle.model.item.armor.ArmorItem;


public class Stick extends WeaponItem {

  public static final String file = "/src/main/resources/view/item/stick.txt";

  public Stick() {

    super(8, AttackType.MAGIC);
    itemStats = new WeaponItemStats(1, 15, 10, 6, 20,
        10);
  }

  public Stick(int level) {
    super(8, AttackType.MAGIC);
    itemStats = getItemStatsByLevel(level);
  }

  @Override
  public DrawInstance getDrawInstance() {
    return DrawInstanceFactory.get(file);
  }
}
