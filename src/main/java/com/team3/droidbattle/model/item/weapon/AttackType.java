package com.team3.droidbattle.model.item.weapon;

public enum AttackType {
  MAGIC(8), SWORD(3), BLASTER(8), TOOTH(2);

  AttackType(int maxDistance) {
    this.maxDistance = maxDistance;
  }

  private int maxDistance;

  public int getMaxDistance() {
    return maxDistance;
  }
}
