package com.team3.droidbattle.model.item.armor;

import com.team3.droidbattle.model.DrawInstance;
import com.team3.droidbattle.model.item.ItemType;

public class Pants extends ArmorItem {

  public Pants() {
    itemStats = new ArmorItemStats(1,5,5,5,0);
  }

  public Pants(int level) {
    itemStats = getItemStatsByLevel(level);
  }

  @Override
  protected ArmorItemStats getItemStatsByLevel(int level) {
    ArmorItemStats stats = super.getItemStatsByLevel(level);

    stats.setBrain(0);
    stats.setPower(stats.getPower() / 2);
    stats.setHp(stats.getHp() / 2);
    stats.setAgility(stats.getAgility() / 2);

    return stats;
  }

  @Override
  public DrawInstance getDrawInstance() {
    return null;
  }

  @Override
  public ItemType getType() {
    return ItemType.PANTS;
  }
}
