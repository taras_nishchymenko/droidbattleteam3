package com.team3.droidbattle.model.item.weapon;

import com.team3.droidbattle.model.DrawInstance;
import com.team3.droidbattle.model.DrawInstanceFactory;
import com.team3.droidbattle.model.item.armor.ArmorItem;


public class Blaster extends WeaponItem {

  public static final String file = "/src/main/resources/view/item/blaster.txt";

  public Blaster() {

    super(8, AttackType.BLASTER);
    itemStats = new WeaponItemStats(1, 15, 10, 6, 20, 10);

  }

  public Blaster(int level) {
    super(8, AttackType.BLASTER);
    itemStats = getItemStatsByLevel(level);
  }

  @Override
  public DrawInstance getDrawInstance() {
    return DrawInstanceFactory.get(file);
  }

}
