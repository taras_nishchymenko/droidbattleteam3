package com.team3.droidbattle.model.item.weapon;

import com.team3.droidbattle.model.DrawInstance;
import com.team3.droidbattle.model.DrawInstanceFactory;
import com.team3.droidbattle.model.item.armor.ArmorItem;


public class Sword extends WeaponItem {

  public static final String file = "/src/main/resources/view/item/sword.txt";

  public Sword() {
    super(3, AttackType.SWORD);
    itemStats = new WeaponItemStats(1, 15, 10, 6, 20,
        10);
  }

  public Sword(int level) {
    super(3, AttackType.SWORD);
    itemStats = getItemStatsByLevel(level);
  }

  @Override
  public DrawInstance getDrawInstance() {
    return DrawInstanceFactory.get(file);
  }

}
