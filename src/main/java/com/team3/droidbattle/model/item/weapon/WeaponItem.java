package com.team3.droidbattle.model.item.weapon;

import com.team3.droidbattle.model.item.Item;
import com.team3.droidbattle.model.item.ItemType;
import com.team3.droidbattle.model.item.weapon.WeaponItemStats.WeaponItemStatsBuilder;
import com.team3.droidbattle.util.Random;

public abstract class WeaponItem extends Item {

  protected WeaponItemStats itemStats;
  protected int maxDistance;
  protected AttackType damageType;

  public WeaponItem(int maxDistance, AttackType attackType) {
    this.maxDistance = maxDistance;
    this.damageType = attackType;
  }

  public WeaponItemStats getItemStats() {
    return itemStats;
  }

  public AttackType getAttackType() {
    return damageType;
  }

  protected WeaponItemStats getItemStatsByLevel(int level) {
    WeaponItemStats weaponItemStats;

    WeaponItemStatsBuilder builder = WeaponItemStatsBuilder.aWeaponItemStats();

    int damageIndex = level * (8 + Random.inRange(0, 4));

    weaponItemStats = builder
        .withCritDamage((int) (damageIndex * 1.5))
        .withCritDamageChance(Random.inRange(1, 20))
        .withDistance(Random.inRange(1, maxDistance))
        .withLevel(level)
        .withMaxDamage((int) (damageIndex * 1.2))
        .withMinDamage((int) (damageIndex * 0.8))
        .build();

    return weaponItemStats;
  }

  public int getMaxDistance() {
    return maxDistance;
  }

  @Override
  public ItemType getType() {
    return ItemType.WEAPON;
  }
}
