package com.team3.droidbattle.model.item.armor;

import com.team3.droidbattle.model.DrawInstance;
import com.team3.droidbattle.model.item.ItemType;

public class Hat extends ArmorItem {

  public Hat() {
    itemStats = new ArmorItemStats(1,0, 5, 0, 10);
  }

  public Hat(int level) {
    itemStats = getItemStatsByLevel(level);
  }

  @Override
  protected ArmorItemStats getItemStatsByLevel(int level) {
    ArmorItemStats stats = super.getItemStatsByLevel(level);

    stats.setPower(0);
    stats.setAgility(0);
    stats.setHp(stats.getHp() / 2);

    return stats;
  }

  @Override
  public DrawInstance getDrawInstance() {
    return null;
  }

  @Override
  public ItemType getType() {
    return ItemType.HAT;
  }
}
