package com.team3.droidbattle.model.item.armor;

public class ArmorItemStats {

  private int level;
  private int power;
  private int hp;
  private int agility;
  private int brain;

  public ArmorItemStats(int level, int power, int hp, int agility, int brain) {
    this.level = level;
    this.power = power;
    this.hp = hp;
    this.agility = agility;
    this.brain = brain;
  }

  public int getPower() {
    return power;
  }

  public int getHp() {
    return hp;
  }

  public int getAgility() {
    return agility;
  }

  public int getBrain() {
    return brain;
  }

  public void setPower(int power) {
    this.power = power;
  }

  public void setHp(int hp) {
    this.hp = hp;
  }

  public void setAgility(int agility) {
    this.agility = agility;
  }

  public void setBrain(int brain) {
    this.brain = brain;
  }

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }


  public static final class ArmorItemStatsBuilder {

    private int level;
    private int power;
    private int hp;
    private int agility;
    private int brain;

    private ArmorItemStatsBuilder() {
    }

    public static ArmorItemStatsBuilder anArmorItemStats() {
      return new ArmorItemStatsBuilder();
    }

    public ArmorItemStatsBuilder withLevel(int level) {
      this.level = level;
      return this;
    }

    public ArmorItemStatsBuilder withPower(int power) {
      this.power = power;
      return this;
    }

    public ArmorItemStatsBuilder withHp(int hp) {
      this.hp = hp;
      return this;
    }

    public ArmorItemStatsBuilder withAgility(int agility) {
      this.agility = agility;
      return this;
    }

    public ArmorItemStatsBuilder withBrain(int brain) {
      this.brain = brain;
      return this;
    }

    public ArmorItemStats build() {
      return new ArmorItemStats(level, power, hp, agility, brain);
    }
  }
}
