package com.team3.droidbattle.model.item.armor;

import com.team3.droidbattle.model.item.Item;
import com.team3.droidbattle.model.item.armor.ArmorItemStats.ArmorItemStatsBuilder;
import com.team3.droidbattle.util.Random;

public abstract class ArmorItem extends Item {

  protected ArmorItemStats itemStats;

  public ArmorItemStats getItemStats() {
    return itemStats;
  }

  protected ArmorItemStats getItemStatsByLevel(int level) {
    ArmorItemStats itemStats;

    int statIndex = level * 5;
    int lower = statIndex - 4;
    int higher = statIndex + 4;

    itemStats = ArmorItemStatsBuilder.anArmorItemStats()
        .withLevel(level)
        .withAgility(Random.inRange(lower, higher))
        .withBrain(Random.inRange(lower, higher))
        .withHp(Random.inRange(lower, higher))
        .withPower(Random.inRange(lower, higher))
        .build();

    return itemStats;
  }
}
