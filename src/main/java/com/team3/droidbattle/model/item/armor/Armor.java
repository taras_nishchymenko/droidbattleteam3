package com.team3.droidbattle.model.item.armor;

import com.team3.droidbattle.model.DrawInstance;
import com.team3.droidbattle.model.item.ItemType;

public class Armor extends ArmorItem {

  public Armor() {
    itemStats = new ArmorItemStats(1, 10, 10,0,0);
  }

  public Armor(int level) {
    itemStats = getItemStatsByLevel(level);
  }

  @Override
  protected ArmorItemStats getItemStatsByLevel(int level) {
    ArmorItemStats stats = super.getItemStatsByLevel(level);

    stats.setAgility(0);
    stats.setBrain(0);

    return stats;
  }

  @Override
  public DrawInstance getDrawInstance() {
    return null;
  }

  @Override
  public ItemType getType() {
    return ItemType.ARMOR;
  }
}
