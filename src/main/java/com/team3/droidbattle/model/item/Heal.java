package com.team3.droidbattle.model.item;

import com.team3.droidbattle.model.DrawInstance;

public class Heal extends Item {

  private int hpPercent;

  public Heal(int hpPercent) {
    this.hpPercent = hpPercent;
  }

  public int getHpPercent() {
    return hpPercent;
  }

  @Override
  public DrawInstance getDrawInstance() {
    return null;
  }

  @Override
  public ItemType getType() {
    return ItemType.HEAL;
  }
}
