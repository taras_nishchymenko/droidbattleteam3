package com.team3.droidbattle.model;

public interface Drawable {

  DrawInstance getDrawInstance();

}
