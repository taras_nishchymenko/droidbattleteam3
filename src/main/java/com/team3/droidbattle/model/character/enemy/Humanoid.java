package com.team3.droidbattle.model.character.enemy;

import com.team3.droidbattle.model.character.Attack;
import com.team3.droidbattle.model.DrawInstance;
import com.team3.droidbattle.model.DrawInstanceFactory;
import com.team3.droidbattle.model.character.AttackEffectivity;
import com.team3.droidbattle.model.character.AttackResult;
import com.team3.droidbattle.model.item.weapon.AttackType;
import com.team3.droidbattle.util.Random;

public class Humanoid extends Enemy {

  public static final String file = "/src/main/resources/view/character/humanoid.txt";

  public Humanoid(int level) {
    super("Humanoid", AttackType.SWORD, level);
  }

  @Override
  public DrawInstance getDrawInstance() {
    return DrawInstanceFactory.get(file);
  }

  public AttackResult getAttack(Attack attack) {
    AttackResult result = new AttackResult();
    result.setEffectivity(AttackEffectivity.NORMAL);
    result.setKilled(false);
    result.setMiss(false);

    int missChance = Random.inRange(0, 100);

    if (missChance <= stats.getAgility() / 10) {
      result.setMiss(true);
      return result;
    }

    int attackPower = attack.getAttackPower();

    if (attack.getAttackType() == AttackType.SWORD) {
      attackPower = (int) (attack.getAttackPower() * 0.8);
      result.setEffectivity(AttackEffectivity.LOW);
    }

    int killedXp = getAttack(attackPower, stats);
    if (killedXp > 0) {
      result.setKilled(true);
      result.setChest(randomChest());
    }
    result.setXp(killedXp);

    result.setDamageCaused(attackPower);

    return result;
  }
}
