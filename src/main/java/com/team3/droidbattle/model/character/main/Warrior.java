package com.team3.droidbattle.model.character.main;

import com.team3.droidbattle.model.character.Attack;
import com.team3.droidbattle.model.DrawInstance;
import com.team3.droidbattle.model.DrawInstanceFactory;
import com.team3.droidbattle.model.character.AttackEffectivity;
import com.team3.droidbattle.model.character.AttackResult;
import com.team3.droidbattle.model.character.CharacterStats.CharacterStatsBuilder;
import com.team3.droidbattle.model.item.weapon.AttackType;
import com.team3.droidbattle.model.item.weapon.Sword;
import com.team3.droidbattle.util.Random;


public class Warrior extends MainCharacter {

  public static final String file = "/src/main/resources/view/character/warrior.txt";

  public Warrior(String name) {
    super(name, AttackType.SWORD, new Sword());
    characterStats = CharacterStatsBuilder.aCharacterStats()
        .withAgility(100)
        .withBrain(100)
        .withHp(100)
        .withMaxHp(100)
        .withPower(120)
        .build();
    calculateStats();
  }

  @Override
  protected void levelUp() {
    characterStats.increaseOnLevelUp(10, 2, 40, 4);
    calculateStats();
  }

  @Override
  protected void calculateStats() {
    calculateFullStats();
    stats.setMaxDamage((int) (stats.getMaxDamage() * 1.05));
    stats.setMinDamage((int) (stats.getMinDamage() * 1.05));
  }

  @Override
  public DrawInstance getDrawInstance() {
    return DrawInstanceFactory.get(file);
  }

  @Override
  public AttackResult getAttack(Attack attack) {
    AttackResult result = new AttackResult();
    result.setKilled(false);
    result.setMiss(false);

    int missChance = Random.inRange(0, 100);

    if (missChance <= stats.getAgility() / 10) {
      result.setMiss(true);
      return result;
    }

    result.setEffectivity(AttackEffectivity.NORMAL);
    int attackPower = attack.getAttackPower();

    if (attack.getAttackType() == AttackType.SWORD) {
      attackPower = (int) (attack.getAttackPower() * 0.9);
      result.setEffectivity(AttackEffectivity.LOW);
    }
    if (attack.getAttackType() == AttackType.MAGIC) {
      attackPower = (int) (attack.getAttackPower() * 1.1);
      result.setEffectivity(AttackEffectivity.HIGH);
    }

    int killedXp = getAttack(attackPower, stats);
    if (killedXp > 0) {
      result.setKilled(true);
    }

    result.setXp(killedXp);
    result.setDamageCaused(attackPower);

    return result;
  }
}
