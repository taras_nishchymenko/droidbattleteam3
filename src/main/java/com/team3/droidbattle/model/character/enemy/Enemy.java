package com.team3.droidbattle.model.character.enemy;

import com.team3.droidbattle.model.Chest;
import com.team3.droidbattle.model.character.Character;
import com.team3.droidbattle.model.character.Level;
import com.team3.droidbattle.model.character.main.FullStats;
import com.team3.droidbattle.model.character.main.FullStats.FullStatsBuilder;
import com.team3.droidbattle.model.item.weapon.AttackType;
import com.team3.droidbattle.util.Random;

public abstract class Enemy extends Character {

  protected Enemy(String name, AttackType attackType, int level) {
    super(name);
    this.attackType = attackType;
    this.level = new Level(level);
    statsByLevel(level);
  }

  public int getLevel() {
    return level.getLevel();
  }

  protected Chest randomChest() {
    return new Chest(getLevel());
  }

  private void statsByLevel(int level) {

    int range = 5 * level;
    int awgHp = 40 * level;
    int hp = 200 + Random.inRange(awgHp - range, awgHp + range);

    int awgStat = 10 * level;
    int damage = Random.inRange((int) (0.8*(20 * level)), (int) (1.2*(20 * level)));

    stats = FullStatsBuilder.aFullStats()
        .withAgility(100 + Random.inRange(awgStat - range, awgStat + range))
        .withBrain(100 + Random.inRange(awgStat - range, awgStat + range))
        .withCritDamage((int) (damage*1.5))
        .withCritDamageChance(Random.inRange(2, 20))
        .withDistance(Random.inRange(1, attackType.getMaxDistance()))
        .withHp(hp)
        .withMaxDamage((int) (damage*1.2))
        .withMaxHp(hp)
        .withMinDamage((int) (damage*0.8))
        .withPower(100 + Random.inRange(awgStat - range, awgStat + range))
        .build();
  }

  public FullStats getFullStats() {
    return stats;
  }
}
