package com.team3.droidbattle.model.character;

import com.team3.droidbattle.model.item.weapon.AttackType;

public class Attack {

  private int attackPower;
  private boolean isCrit;
  private AttackType attackType;

  public boolean isCrit() {
    return isCrit;
  }

  public int getAttackPower() {
    return attackPower;
  }

  public AttackType getAttackType() {
    return attackType;
  }

  public void setAttackPower(int attackPower) {
    this.attackPower = attackPower;
  }

  public void setCrit(boolean crit) {
    isCrit = crit;
  }

  public void setAttackType(AttackType attackType) {
    this.attackType = attackType;
  }
}
