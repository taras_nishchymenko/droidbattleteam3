package com.team3.droidbattle.model.character;

import com.team3.droidbattle.model.Chest;

public class AttackResult {

  private int damageCaused;
  private boolean miss;
  private boolean killed;
  private int xp;
  private AttackEffectivity effectivity;
  private Chest chest;

  public Chest getChest() {
    return chest;
  }

  public void setChest(Chest chest) {
    this.chest = chest;
  }

  public int getXp() {
    return xp;
  }

  public void setXp(int xp) {
    this.xp = xp;
  }

  public int getDamageCaused() {
    return damageCaused;
  }

  public boolean isKilled() {
    return killed;
  }

  public void setKilled(boolean killed) {
    this.killed = killed;
  }

  public void setDamageCaused(int damageCaused) {
    this.damageCaused = damageCaused;
  }

  public boolean isMiss() {
    return miss;
  }

  public void setMiss(boolean miss) {
    this.miss = miss;
  }

  public AttackEffectivity getEffectivity() {
    return effectivity;
  }

  public void setEffectivity(AttackEffectivity effectivity) {
    this.effectivity = effectivity;
  }
}
