package com.team3.droidbattle.model.character.main;

public enum CharacterType {
  MAGE, SHOOTER, WARRIOR
}
