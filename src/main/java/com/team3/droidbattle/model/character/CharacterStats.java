package com.team3.droidbattle.model.character;

public class CharacterStats {

  protected int power;
  protected int brain;
  protected int hp;
  protected int maxHp;
  protected int agility;

  public int getPower() {
    return power;
  }

  public void setPower(int power) {
    this.power = power;
  }

  public int getBrain() {
    return brain;
  }

  public void setBrain(int brain) {
    this.brain = brain;
  }

  public int getHp() {
    return hp;
  }

  public void setHp(int hp) {
    this.hp = hp;
  }

  public int getMaxHp() {
    return maxHp;
  }

  public void setMaxHp(int maxHp) {
    this.maxHp = maxHp;
  }

  public int getAgility() {
    return agility;
  }

  public void setAgility(int agility) {
    this.agility = agility;
  }

  public void increaseOnLevelUp(int power, int brain, int hp, int agility) {
    this.power += power;
    this.maxHp += hp;
    this.hp = maxHp;
    this.brain += brain;
    this.agility += agility;
  }


  public static final class CharacterStatsBuilder {

    private int power;
    private int brain;
    private int hp;
    private int maxHp;
    private int agility;

    private CharacterStatsBuilder() {
    }

    public static CharacterStatsBuilder aCharacterStats() {
      return new CharacterStatsBuilder();
    }

    public CharacterStatsBuilder withPower(int power) {
      this.power = power;
      return this;
    }

    public CharacterStatsBuilder withBrain(int brain) {
      this.brain = brain;
      return this;
    }

    public CharacterStatsBuilder withHp(int hp) {
      this.hp = hp;
      return this;
    }

    public CharacterStatsBuilder withMaxHp(int maxHp) {
      this.maxHp = maxHp;
      return this;
    }

    public CharacterStatsBuilder withAgility(int agility) {
      this.agility = agility;
      return this;
    }

    public CharacterStats build() {
      CharacterStats characterStats = new CharacterStats();
      characterStats.brain = this.brain;
      characterStats.maxHp = this.maxHp;
      characterStats.power = this.power;
      characterStats.hp = this.hp;
      characterStats.agility = this.agility;
      return characterStats;
    }
  }
}
