package com.team3.droidbattle.model.character.main;

import com.team3.droidbattle.model.character.CharacterStats;

public class FullStats extends CharacterStats {

  protected int maxDamage;
  protected int minDamage;
  protected int distance;
  protected int critDamage;
  protected int critDamageChance;

  public int getMaxDamage() {
    return maxDamage;
  }

  public void setMaxDamage(int maxDamage) {
    this.maxDamage = maxDamage;
  }

  public int getMinDamage() {
    return minDamage;
  }

  public void setMinDamage(int minDamage) {
    this.minDamage = minDamage;
  }

  public int getDistance() {
    return distance;
  }

  public void setDistance(int distance) {
    this.distance = distance;
  }

  public int getCritDamage() {
    return critDamage;
  }

  public void setCritDamage(int critDamage) {
    this.critDamage = critDamage;
  }

  public int getCritDamageChance() {
    return critDamageChance;
  }

  public void setCritDamageChance(int critDamageChance) {
    this.critDamageChance = critDamageChance;
  }

  public static final class FullStatsBuilder {

    private int maxDamage;
    private int minDamage;
    private int distance;
    private int critDamage;
    private int critDamageChance;
    private int power;
    private int hp;
    private int maxHp;
    private int agility;
    private int brain;

    private FullStatsBuilder() {
    }

    public static FullStatsBuilder aFullStats() {
      return new FullStatsBuilder();
    }

    public FullStatsBuilder withMaxDamage(int maxDamage) {
      this.maxDamage = maxDamage;
      return this;
    }

    public FullStatsBuilder withMinDamage(int minDamage) {
      this.minDamage = minDamage;
      return this;
    }

    public FullStatsBuilder withDistance(int distance) {
      this.distance = distance;
      return this;
    }

    public FullStatsBuilder withCritDamage(int critDamage) {
      this.critDamage = critDamage;
      return this;
    }

    public FullStatsBuilder withCritDamageChance(int critDamageChance) {
      this.critDamageChance = critDamageChance;
      return this;
    }

    public FullStatsBuilder withPower(int power) {
      this.power = power;
      return this;
    }

    public FullStatsBuilder withHp(int hp) {
      this.hp = hp;
      return this;
    }

    public FullStatsBuilder withAgility(int agility) {
      this.agility = agility;
      return this;
    }

    public FullStatsBuilder withBrain(int brain) {
      this.brain = brain;
      return this;
    }

    public FullStatsBuilder withMaxHp(int hp) {
      this.maxHp = hp;
      return this;
    }

    public FullStats build() {
      FullStats fullStats = new FullStats();
      fullStats.hp = this.hp;
      fullStats.brain = this.brain;
      fullStats.maxDamage = this.maxDamage;
      fullStats.agility = this.agility;
      fullStats.distance = this.distance;
      fullStats.critDamageChance = this.critDamageChance;
      fullStats.minDamage = this.minDamage;
      fullStats.power = this.power;
      fullStats.critDamage = this.critDamage;
      fullStats.maxHp = this.maxHp;
      return fullStats;
    }
  }

  @Override
  public String toString() {
    return "FullStats{" +
        "maxDamage=" + maxDamage +
        ", minDamage=" + minDamage +
        ", distance=" + distance +
        ", critDamage=" + critDamage +
        ", critDamageChance=" + critDamageChance +
        ", power=" + power +
        ", brain=" + brain +
        ", hp=" + hp +
        ", maxHp=" + maxHp +
        ", agility=" + agility +
        '}';
  }
}
