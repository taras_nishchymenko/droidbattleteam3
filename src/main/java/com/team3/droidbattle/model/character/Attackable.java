package com.team3.droidbattle.model.character;

public interface Attackable {

  AttackResult getAttack(Attack attack);

  Attack attack();
}
