package com.team3.droidbattle.model.character.main;

import com.team3.droidbattle.model.character.Character;
import com.team3.droidbattle.model.character.Level;
import com.team3.droidbattle.model.character.main.FullStats.FullStatsBuilder;
import com.team3.droidbattle.model.item.Heal;
import com.team3.droidbattle.model.item.Putable;
import com.team3.droidbattle.model.item.armor.Armor;
import com.team3.droidbattle.model.item.armor.ArmorItem;
import com.team3.droidbattle.model.item.armor.ArmorItemStats;
import com.team3.droidbattle.model.item.armor.Hat;
import com.team3.droidbattle.model.item.armor.Pants;
import com.team3.droidbattle.model.item.weapon.AttackType;
import com.team3.droidbattle.model.item.weapon.WeaponItem;
import java.util.Arrays;
import java.util.List;

public abstract class MainCharacter extends Character {

  protected Armor armor;
  protected Hat hat;
  protected Pants pants;
  protected WeaponItem weapon;
  protected AttackType preferredType;

  protected MainCharacter(String name, AttackType preferredType, WeaponItem weapon) {
    super(name);
    level = new Level();
    armor = new Armor();
    hat = new Hat();
    pants = new Pants();
    this.preferredType = preferredType;
    this.weapon = weapon;
    attackType = weapon.getAttackType();
  }

  public int getRange() {
    return weapon.getItemStats().getDistance();
  }

  public FullStats getStats() {
    return stats;
  }

  protected void calculateFullStats() {

    List<ArmorItem> items = Arrays.asList(armor, hat, pants);

    int agility = characterStats.getAgility();
    int brain = characterStats.getBrain();
    int hp = characterStats.getHp();
    int maxHp = characterStats.getMaxHp();
    int power = characterStats.getPower();

    for (ArmorItem item : items) {
      ArmorItemStats stats = item.getItemStats();
      agility += stats.getAgility();
      brain += stats.getBrain();
      hp += stats.getHp();
      maxHp += stats.getHp();
      power += stats.getPower();
    }

    double weaponBonus = 1.;

    if (weapon.getAttackType() == preferredType) {
      weaponBonus = 1.3;
    }

    int weaponDistance = weapon.getItemStats().getDistance();
    int critDamage = (int) (power + weapon.getItemStats().getCritDamage() * weaponBonus);
    int critDamageChance = weapon.getItemStats().getCritDamageChance();
    int maxDamage = (int) (power + weapon.getItemStats().getMaxDamage() * weaponBonus);
    int minDamage = (int) (power + weapon.getItemStats().getMinDamage() * weaponBonus);

    stats = FullStatsBuilder.aFullStats()
        .withAgility(agility)
        .withBrain(brain)
        .withCritDamage(critDamage)
        .withCritDamageChance(critDamageChance)
        .withDistance(weaponDistance)
        .withHp(hp)
        .withMaxDamage(maxDamage)
        .withMaxHp(maxHp)
        .withMinDamage(minDamage)
        .withPower(power)
        .build();
  }

  public void put(Putable item) {
    switch (item.getType()) {
      case HAT:
        hat = (Hat) item;
        break;
      case ARMOR:
        armor = (Armor) item;
        break;
      case PANTS:
        pants = (Pants) item;
        break;
      case WEAPON:
        weapon = (WeaponItem) item;
        attackType = ((WeaponItem) item).getAttackType();
        break;
      case HEAL:
        heal((Heal) item);
        break;
    }

    calculateStats();
  }

  public void increaseXp(int xp) {
    int levelsToUp = level.increaseXp(xp);
    if (levelsToUp == 0) {
      return;
    } else {
      while (levelsToUp > 0) {
        levelUp();
        levelsToUp--;
      }
    }
  }

  public void heal(Heal heal) {
    if (heal.getHpPercent() + characterStats.getHp() >= characterStats.getMaxHp()) {
      characterStats.setHp(characterStats.getMaxHp());
    } else {
      characterStats.setHp(heal.getHpPercent() + characterStats.getHp());
    }
  }

  public int getLevel() {
    return level.getLevel();
  }

  protected abstract void levelUp();

  protected abstract void calculateStats();


}
