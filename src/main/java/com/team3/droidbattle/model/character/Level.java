package com.team3.droidbattle.model.character;

public class Level {

  private int level;
  private int xp;
  private int toNextLevel;

  public Level() {
    level = 1;
    xp = 0;
    toNextLevel = 100;
  }

  public Level(int level) {
    this.level = level;
  }

  private void nextLevel() {
    level += 1;
    toNextLevel = calculateXPToNextLevel();
  }

  private int calculateXPToNextLevel() {
    return toNextLevel * 2;
  }

  public int increaseXp(int xp) {
    if (this.xp + xp >= toNextLevel) {
      int nextLevel = toNextLevel;
      nextLevel();
      return increaseXp((this.xp + xp) - nextLevel) + 1;
    } else {
      this.xp += xp;
      return 0;
    }
  }

  public int getLevel() {
    return level;
  }

  public int getXp() {
    return xp;
  }

  public int getToNextLevel() {
    return toNextLevel;
  }
}
