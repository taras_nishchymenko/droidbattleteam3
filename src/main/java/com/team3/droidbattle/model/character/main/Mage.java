package com.team3.droidbattle.model.character.main;

import com.team3.droidbattle.model.DrawInstance;
import com.team3.droidbattle.model.DrawInstanceFactory;
import com.team3.droidbattle.model.character.Attack;
import com.team3.droidbattle.model.character.AttackEffectivity;
import com.team3.droidbattle.model.character.AttackResult;
import com.team3.droidbattle.model.character.CharacterStats.CharacterStatsBuilder;
import com.team3.droidbattle.model.item.weapon.AttackType;
import com.team3.droidbattle.model.item.weapon.Stick;
import com.team3.droidbattle.util.Random;

public class Mage extends MainCharacter {

  private static final String file = "/src/main/resources/view/character/mage.txt";

  public Mage(String name) {
    super(name, AttackType.MAGIC, new Stick());
    characterStats = CharacterStatsBuilder.aCharacterStats()
        .withAgility(100)
        .withBrain(120)
        .withHp(100)
        .withMaxHp(100)
        .withPower(100)
        .build();
    calculateStats();
  }

  @Override
  protected void levelUp() {
    characterStats.increaseOnLevelUp(3, 10, 25, 3);
    calculateStats();
  }

  @Override
  protected void calculateStats() {
    calculateFullStats();
    stats.setCritDamage((int) (stats.getCritDamage() * 1.25));
    stats.setCritDamageChance((int) (stats.getCritDamageChance() * 2.));
  }

  @Override
  public DrawInstance getDrawInstance() {
    return DrawInstanceFactory.get(file);
  }

  @Override
  public AttackResult getAttack(Attack attack) {
    AttackResult result = new AttackResult();
    result.setKilled(false);
    result.setMiss(false);

    int missChance = Random.inRange(0, 100);

    if (missChance <= stats.getAgility() / 10) {
      result.setMiss(true);
      return result;
    }

    result.setEffectivity(AttackEffectivity.NORMAL);
    int attackPower = attack.getAttackPower();

    if (attack.getAttackType() == AttackType.MAGIC) {
      attackPower = (int) (attack.getAttackPower() * 0.9);
      result.setEffectivity(AttackEffectivity.LOW);
    }
    if (attack.getAttackType() == AttackType.SWORD) {
      attackPower = (int) (attack.getAttackPower() * 1.1);
      result.setEffectivity(AttackEffectivity.HIGH);
    }

    int killedXp = getAttack(attackPower, stats);
    if (killedXp > 0) {
      result.setKilled(true);
    }

    result.setXp(killedXp);

    result.setDamageCaused(attackPower);

    return result;
  }
}
