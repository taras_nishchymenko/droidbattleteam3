package com.team3.droidbattle.model.character.enemy;

import com.team3.droidbattle.model.character.Attack;
import com.team3.droidbattle.model.DrawInstance;
import com.team3.droidbattle.model.DrawInstanceFactory;
import com.team3.droidbattle.model.character.AttackEffectivity;
import com.team3.droidbattle.model.character.AttackResult;
import com.team3.droidbattle.model.item.weapon.AttackType;
import com.team3.droidbattle.util.Random;


public class Droid extends Enemy {

  private static final String file = "/src/main/resources/view/character/droid.txt";


  public Droid(int level) {
    super("Droid", AttackType.BLASTER, level);
  }

  @Override
  public DrawInstance getDrawInstance() {
    return DrawInstanceFactory.get(file);
  }

  @Override
  public AttackResult getAttack(Attack attack) {
    AttackResult result = new AttackResult();
    result.setEffectivity(AttackEffectivity.NORMAL);
    result.setKilled(false);
    result.setMiss(false);

    int missChance = Random.inRange(0, 100);

    if (missChance <= stats.getAgility() / 100) {
      result.setMiss(true);
      return result;
    }

    int attackPower = attack.getAttackPower();

    if (attack.getAttackType() == AttackType.MAGIC) {
      attackPower = (int) (attack.getAttackPower() * 0.8);
      result.setEffectivity(AttackEffectivity.LOW);
    }
    if (attack.getAttackType() == AttackType.BLASTER) {
      attackPower = (int) (attack.getAttackPower() * 1.2);
      result.setEffectivity(AttackEffectivity.HIGH);
    }

    int killedXp = getAttack(attackPower, stats);
    if (killedXp > 0) {
      result.setKilled(true);
      result.setChest(randomChest());
    }

    result.setXp(killedXp);
    result.setDamageCaused(attackPower);

    return result;
  }


}
