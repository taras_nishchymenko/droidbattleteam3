package com.team3.droidbattle.model.character;

import com.team3.droidbattle.model.Drawable;
import com.team3.droidbattle.model.character.main.FullStats;
import com.team3.droidbattle.model.item.weapon.AttackType;
import com.team3.droidbattle.util.Random;

public abstract class Character implements Drawable, Attackable {

  protected String name;
  protected CharacterStats characterStats;
  protected Level level;
  protected AttackType attackType;
  protected FullStats stats;

  protected Character(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  protected int getAttack(int power, CharacterStats stats) {
    if (stats.getHp() - power <= 0) {
      stats.setHp(0);
      return getXpByLevel();
    } else {
      stats.setHp(stats.getHp() - power);
      return 0;
    }
  }

  protected int getXpByLevel() {
    int xpIndex = level.getLevel() * 40;
    return Random.inRange(xpIndex * 0.8, xpIndex * 1.2);
  }

  @Override
  public Attack attack() {
    Attack attack = new Attack();
    attack.setAttackType(attackType);
    attack.setCrit(false);

    int randomChance = Random.inRange(0, 100);

    if (randomChance <= stats.getCritDamageChance()) {
      attack.setAttackPower(stats.getCritDamage());
      attack.setCrit(true);
    } else {
      int attackPower = Random.inRange(stats.getMinDamage(), stats.getMaxDamage());
      attack.setAttackPower(attackPower);
    }

    return attack;
  }

}