package com.team3.droidbattle.model;

public class Tree implements Drawable {

  private final String file =
      "/src/main/resources/view/tree.txt";

  @Override
  public DrawInstance getDrawInstance() {
    return DrawInstanceFactory.get(file);
  }
}
