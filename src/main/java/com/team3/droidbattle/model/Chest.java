package com.team3.droidbattle.model;

import com.team3.droidbattle.model.item.Heal;
import com.team3.droidbattle.model.item.Item;
import com.team3.droidbattle.model.item.ItemType;
import com.team3.droidbattle.model.item.armor.Armor;
import com.team3.droidbattle.model.item.armor.Hat;
import com.team3.droidbattle.model.item.armor.Pants;
import com.team3.droidbattle.model.item.weapon.AttackType;
import com.team3.droidbattle.model.item.weapon.Blaster;
import com.team3.droidbattle.model.item.weapon.Stick;
import com.team3.droidbattle.model.item.weapon.Sword;
import com.team3.droidbattle.util.Random;

public class Chest implements Drawable {

  private Item loot;
  private Heal heal = new Heal(25);
  private static final String file = "/src/main/resources/view/item/chest.txt";

  public Chest(int level) {
    int randomLevel = Random.inRange(level, level + 4);
    int randomType = Random.inRange(1, ItemType.values().length);
    loot = getRandomItemByType(randomType, randomLevel);
  }

  public void setLoot(Item loot) {
    this.loot = loot;
  }

  public void setHeal(Heal heal) {
    this.heal = heal;
  }

  public Item getLoot() {
    return loot;
  }

  public Heal getHeal() {
    return heal;
  }

  private Item getRandomItemByType(int type, int level) {
    ItemType itemType = ItemType.values()[type - 1];
    switch (itemType) {
      case ARMOR:
        return new Armor(level);
      case HAT:
        return new Hat(level);
      case PANTS:
        return new Pants(level);
      case WEAPON:
        return randomWeaponByLevel(level);
      default:
        return null;
    }
  }

  private Item randomWeaponByLevel(int level) {
    int randomType = Random.inRange(1, AttackType.values().length);
    AttackType attackType = AttackType.values()[randomType - 1];
    switch (attackType) {
      case SWORD:
        return new Sword(level);
      case MAGIC:
        return new Stick(level);
      case BLASTER:
        return new Blaster(level);
      default:
        return null;
    }
  }

  @Override
  public DrawInstance getDrawInstance() {
    return DrawInstanceFactory.get(file);
  }
}
