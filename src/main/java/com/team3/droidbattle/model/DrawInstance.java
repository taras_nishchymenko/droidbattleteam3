package com.team3.droidbattle.model;

import java.util.List;

public class DrawInstance {

  private List<String> lines;

  public List<String> getLines() {
    return lines;
  }

  public void setLines(List<String> lines) {
    this.lines = lines;
  }
}
