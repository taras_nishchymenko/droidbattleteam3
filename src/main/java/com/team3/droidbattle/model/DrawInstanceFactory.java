package com.team3.droidbattle.model;

import com.team3.droidbattle.model.character.main.Mage;
import com.team3.droidbattle.util.StringUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DrawInstanceFactory {

  public static DrawInstance get(String filePath) {

    DrawInstance drawInstance = new DrawInstance();
    List<String> lines = new ArrayList<>();

    File file = new File("/home/taras/IdeaProjects/droidbattleteam3" + filePath);

    try (FileReader fileReader = new FileReader(file)) {

      BufferedReader reader = new BufferedReader(fileReader);
      String line = reader.readLine();

      while (line != null) {
        line = StringUtil.rightPadTillSize(line, 33, ' ');
        if (line.length() > 33) {
          line = line.substring(0, 32);
        }
        lines.add(line);
        line = reader.readLine();
      }
    } catch (IOException e) {
      System.out.println("Something very bad happen... [tears]");
    }

    while (lines.size() < 30) {
      lines.add(StringUtil.rightPadTillSize(" ", 32, ' '));
    }

    drawInstance.setLines(lines);
    return drawInstance;
  }
}
